FROM golang:1.19 AS builder
RUN git clone --depth 1 https://github.com/aquasecurity/trivy.git /trivy
WORKDIR /trivy
RUN CGO_ENABLED=0 go build -o /usr/local/bin/trivy cmd/trivy/main.go

FROM alpine:latest
RUN apk --no-cache add ca-certificates git
COPY --from=builder /trivy/main /usr/local/bin/trivy
ENTRYPOINT ["trivy"]
